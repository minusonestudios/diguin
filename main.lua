local anim8 = require 'source/anim8' --importa a biblioteca do love responsavel pelas animaçoes
require('source/audio') --biblioteca pronta para manipulacao de audio
require('source/camera') ---biblioteca pronta para camera seguir o jogador
require('source/aranha') ---inimigo aranha
require('source/cactus') ---inimigo cactus
require('source/coruja') ---inimigo coruja
require('source/diguinho') --personagem principal
require('source/flor') --ponto final da fase
require('source/caixa') --bordas do cenario
require('source/pontuacao') --controla a pontuacao do jogo
require('source/menu') --criacao e verificacao de botoes
require('source/credits') --exibe a tela de créditos
require('source/help') --exibe a tela de ajuda
require('source/submiting') --exibe a tela do gamejolt para enviar score

local GJ = require "source/gamejolt"
local score_tables = {
    main = nil,
    time = nil,
    distance = nil,
    flower = nil
}

--DEFINES
local SUBMIT_MSG = "Your score has been successfully submitted!"
local SUBMIT_ERROR_MSG = "Error while score submittion. Try again."
local SUBMIT_ERROR_FIELDS = "Please complete all fields."
local SUBMIT_ERROR_USER = "Invalid User."
local SUBMIT_SUCESS = "SUBMIT SUCESS"
local SUBMIT_ERROR = "SUBMIT ERROR"
local FONT_PATH = "font/Beauty and the Dutch.ttf"
local DISTANCIA_MINIMA = 600
local FLOR_DISTANCIA_MINIMA = 50
local REFRESH_MINIMO = 5
local MAX_FLOR_REFRESH = 10
local CORUJA_VELOCIDADE_MAXIMA = 485
--END DEFINES

local jogador = {}
local flor = {}
local aranha = {}
local cactus = {}
local coruja = {}
local caixa = {}
local pontuacao = {}
local corujaVelocidade = 200 --Controla a velocidade da coruja
local menu = {}
local credits = {}
local help = {}
local world = nil
local font = nil
local gamestate = ''
local backgroundSong = nil
local backgroudImage = nil
local show = 0
local width = 0
local height = 0
local worldSize = {
  height = 610,
  width = 800
}

--[[-----------------------------------------------------------------------------]]
--[[----------------------- Funcoes privados do jogo ----------------------------]]
--[[-----------------------------------------------------------------------------]]

--Funcao exlusiva para o uso da biblioteca 'camera'
function math.clamp(x, min, max) 
	return x < min and min or (x > max and max or x)
end

--Funcao que termina o jogo caso o jogador esteja morto
local function onEnd()
	if not jogador.alive then
		show=0 --seta o alpha de scenario para 0, assim as imagens de scenario sumirao
		camera:setPosition(0,0) --seta a posicao da camera para o vertice superior esquerdo
		love.audio.stop(backgroundSong) --pausa a musica
	end
end

--Funcao que eh chamada quando dois corpos entram em contato,
--'a'eh o primeiro corpo
--'b' o segundo
--'coll' eh o tipo de colisao
local function beginContact(a, b, coll)

	--colisao do jogador com a flor
	if jogador.alive and flor.active and (a ==jogador.fixture and b == flor.fixture)
	or  (b ==jogador.fixture and a == flor.fixture) then
        love.audio.play("musica/ring.ogg", "stream", false)
        flor.active = false
		pontuacao:pegaFlor()
        --jogador.alive = false
		--gamestate = 'levelup'
	end

	--colisao do jogador com o inimigo
	if jogador.alive and (a == jogador.fixture and (b == coruja.fixture or b == aranha.fixture or b == cactus.fixture))
	or  (b == jogador.fixture and (a == coruja.fixture or a == aranha.fixture or a == cactus.fixture)) then
        love.audio.stop(backgroundSong)
        love.audio.play("musica/crash.ogg", "stream", false)
		love.timer.sleep(1)
		jogador.alive = false
        pontuacao:pararCronometro()
		gamestate = 'submiting'
	end
end

--Funcao que decidira qual acao deve ser tomada para cada evento de botao
local function botaoPressionado (key) --verfica se o botao selecionado foi pressionado
	for i,v in ipairs(menu.buttons) do
    --se a key(tecla) foi return (enter) e o botao estava selecionado ele sera pressionado
		if (key=="return" or key=="click") and v.selected then 
			if v.id=='quit' then
				love.event.quit( ) --caso a identidade for quit entao essa funcao fecha o jogo
            elseif v.id == 'credits' then
                gamestate = 'credits'
            elseif v.id == 'help' then
                gamestate = 'help'
			elseif v.id=='start' then
				gamestate = 'playing' --caso a identidade for start entao o jogo comeca e a musica tambem
				love.audio.play(backgroundSong) --da play no som
                pontuacao:iniciarCronometro()
			end
		end
	end
end

local function submitScore()    
    -- Autentica o jogador
    -- (nome_do_jogador, token)
    local auth = submit_getAuthentication()
	if (auth.token ~= nil and string.len(auth.token) > 0) and (auth.user ~= nil and string.len(auth.user) > 0) then
		if GJ.authUser(auth.user, auth.token) then
			GJ.openSession() 
			GJ.closeSession()
			--ENDJOLT
			local score = {
				main = pontuacao:getMain(),
				flight = pontuacao.distancia,
				time = math.floor(pontuacao.tempoTotal),
				flower = pontuacao.flores
			}						
			score_tables.main = GJ.addScore(score.main, score.main .. "pts", 329039)
			score_tables.flight = GJ.addScore(score.flight, score.flight .. "m", 331553)
			score_tables.time = GJ.addScore(score.time, score.time .. "s", 331554)
			score_tables.flower = GJ.addScore(score.flower, score.flower .. "pts", 331555)								
			if score_tables.main then
				love.window.showMessageBox( SUBMIT_SUCESS, SUBMIT_MSG, "info", true )
			else
				love.window.showMessageBox( SUBMIT_ERROR, SUBMIT_ERROR_MSG, "error", true )
			end 
		else
			love.window.showMessageBox( SUBMIT_ERROR, SUBMIT_ERROR_USER, "error", true )  
		end
	else
		love.window.showMessageBox( SUBMIT_ERROR, SUBMIT_ERROR_FIELDS, "error", true )
	end
end

--Funcao que carrega a fase no jogo
local function iniciarJogo ()
    love.graphics.setBackgroundColor(37, 170, 225) --muda a cor de fundo
	--cria um mundo para o jogo, com fisicas, como por exemplo, gravidade (horizontal ou vertical)
	love.physics.setMeter(64) --define o numero de pixels/metro no mundo
	world = love.physics.newWorld( 0, 100*64, true )
	--cria funcoes que sao chamadas quando corpos no Mundo se colidem
	world:setCallbacks(beginContact, function() collectgarbage() end)
	--a funcao colletgarbage eh, uma funcao da Lua para limpar a memoria de objetos que foram destruidos

    backgroudImage = love.graphics.newImage("imagens/bg.png") 

	--Cria a camera que seguira o jogador
	--camera
	width = love.graphics.getWidth()
	height = love.graphics.getHeight()
	camera:setBounds(0, 0, worldSize.width, height)

	--'show' eh a variavel que controla o alpha do backgroud, ou seja a transparencia, 0 eh transparente e 255 eh opaco
	show = 255

	--funcoes que criarao os elementos do jogo
	jogador = Jogador(world, 100, 100)
	flor = Flor(world, 800)
    caixa = Caixa(world, worldSize)
    pontuacao = Pontuacao(FONT_PATH)
	coruja = Coruja(world, 60000, math.random(Coruja.min_height, Coruja.max_height))
    aranha = Aranha(world, 12000)
    cactus = Cactus(world, 6000)
end

local function submitAction(key)
	for i,v in ipairs(submitionMenu.buttons) do
    --se a key(tecla) foi return (enter) e o botao estava selecionado ele sera pressionado
		if (key=="return" or key=="click") and v.selected then 
			if v.id=='submit' then
				submitScore()
				iniciarJogo()
				gamestate = 'menu'-- volta para o menu				
			elseif v.id=='cancel' then
				iniciarJogo()
				gamestate = 'menu'-- volta para o menu
			end
		end
	end
end

--[[-----------------------------------------------------------------------------]]
--[[------------------ Eventos do Love - Maquina de Estados ---------------------]]
--[[-----------------------------------------------------------------------------]]

--Funcao principal Load (soh roda 1 vez)
function love.load()  
    -- Inicia instancia do Gamejolt
    -- (id_jogo, token_jogo)
    GJ.init(323679, "10250294a3e8a37cab16226c3f232308")
    
    love.math.setRandomSeed(love.timer.getTime())

	--define um tamanho de fonte padrao para ser usado no jogo
	font = love.graphics.newFont(FONT_PATH, 72)
	scoreFont = love.graphics.newFont(FONT_PATH, 24) 

	gamestate = 'menu' --define o primeiro estado do jogo como o MENU
  
  	menu = Menu('imagens/menu.png', FONT_PATH)
  	--botoes do menu
	menu:addButton("start", 370, 370, 'start')
	menu:addButton("credits", 365, 420, 'credits')
    menu:addButton("help", 375, 470, 'help')
    menu:addButton("quit", 375, 520, 'quit')
    credits = Credits(FONT_PATH)
    help = Help(FONT_PATH)
	
	submitionMenu = submit_create(FONT_PATH)

	--carrega um arquivo de musica para o jogo, o tipo stream eh eficiente para musicas de scenario, longas e que se repetem
	backgroundSong = love.audio.play("musica/plain_fields.ogg", "stream", true) -- stream and loop background music
	-- pausa a musica inicialmente
	love.audio.stop(backgroundSong)

	--agora vamos carregar o primeiro nivel do jogo
	iniciarJogo()
end

--Funcao principal Update que atualiza o jogo a cada frame
function love.update(dt) --dt = delta time (tempo entre um frame e outro)
	if gamestate == 'playing' then --caso a fase tenha comecado, os seguintes elementos sao atualizados
		world:update(dt)    
		if jogador.alive then
			
			caixa:update(dt)
			
			jogador:update(dt)
			
			pontuacao:update(jogador.body:getX())
			
            -- Atualiza os limites da camera
            camera:setBounds(0, 0, caixa.borders.right.body:getX(), height)
            -- A camera deverá fixar-se na caixa 
            camera:setPosition(caixa.borders.left.body:getX()) -- A camera deve seguir o ponto mais a esquerda da caixa
			
			--Refresh de Owls position along the game runs   
			coruja:update(dt, corujaVelocidade)
			coruja:respawn(caixa.borders.top.body:getX())
		   
			--Refresh de Spiders position along the game runs
			aranha:update(dt, worldSize)
            aranha:respawn(caixa.borders.top.body:getX())
			   
			--Refresh de Flowers position along the game runs
			flor:respawn(caixa.borders.top.body:getX())
			
 			--Refresh de Cactus position along the game runs   
            cactus:respawn(caixa.borders.top.body:getX())
        end
		--funcao que verifica se o jogador conluiu a fase ou morreu
		onEnd()
	end
end

--Funcao principal de desenho dos objetos
function love.draw()
	if gamestate =='menu' then --caso o jogo esteja no menu, desenhara a tela do menu e os botoes
		menu:drawButtons()
		menu:drawArt()
    elseif gamestate =='credits' then
        credits:draw()
    elseif gamestate =='help' then
        help:draw()
	elseif gamestate =='submiting' then
        submit_draw(pontuacao)	
	else
		camera:set() --foca a camera no jogador
        if jogador.alive then --verifica se o jogador esta vivo, se sim, desenha todos os elementos associados a ele              
            local boxX = caixa.borders.top.body:getX()
            local boxY = caixa.borders.top.body:getY()

            love.graphics.setColor(255, 255, 255, show * 0.7)            
            love.graphics.draw(backgroudImage, boxX, boxY)
            love.graphics.setColor(250, 250, 250, show)
			flor:draw()
			coruja:draw()
            aranha:draw()
            cactus:draw()
			jogador:draw()
			caixa:draw()
            pontuacao:draw(boxX)
		end
		camera:unset()--desfoca a camera da posicao antiga
	end
end

--Funcao da engine Love para capturar evento de teclado
function love.keypressed(key) --funcao do Love para verificar se uma tecla foi pressionada
	if gamestate == 'menu' then --no menu, checa se um botao foi pressionado
		menu:checkSelection(key)
		botaoPressionado(key)
    elseif gamestate == 'credits' or gamestate == 'help' then
        if key == "escape" or key == "return" then
			gamestate = 'menu'
		end
	elseif gamestate == 'submiting' then		
        submit_checkComponents(key) 
		submitAction(key)
	else --em jogo, checa se houve um pedido para voltar ao menu
		if key == "escape" then
            pontuacao:pararCronometro()
			gamestate = 'menu' -- volta para o menu
			love.audio.stop(backgroundSong)
		elseif key == "return" and (not jogador.alive) then
			gamestate = 'submiting'
		end
	end
end

--Funcao da engine Love para capturar evento de mouse
function love.mousepressed(x, y, button)
	if gamestate == 'menu' then --no menu, checa se um botao foi pressionado
		menu:checkClick()
		botaoPressionado("click")
	elseif gamestate == 'submiting' then
		submit_clickOnComponents()
		submitAction("click")		
	end
end

function love.textinput(t) 
    if gamestate == "submiting" then  
       submit_getInput(t)        
    end   
end