local anim8 = require 'source/anim8' --importa a biblioteca do love responsável pelas animacõeses

Coruja = {
	vertices = {
		83.709, 154.624,	
		83.059, 143.130,
		85.520, 136.504,		
		81.541, 129.901,		
		77.768, 112.121,		
		71.565, 95.637,		
		75.620, 77.273,		
		83.680, 65.718,		
		80.178, 50.171,		
		91.453, 60.635,		
		111.045, 61.599,		
		124.562, 51.797,
		122.745, 66.577,
		131.575, 80.656,
		134.022, 96.287,	
		127.551, 111.464,		
		123.344, 126.075,		
		120.496, 135.963,		
		120.793, 153.973
	},
	min_distance = 3500,
	max_distance = 5000,
  min_height = 50, --Ponto minimo (altura) que a coruja será criada
  max_height = 325 --Ponto máximo (altura) que a coruja será criada
}
Coruja.__index = Coruja

setmetatable(Coruja, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})
	 
function Coruja.new(world, px, py) -- carrega as propriedades do inimigo
	
	local self = setmetatable({}, Coruja) --cria um registro com as caracteristicas da coruja
  --cria um corpo para a coruja no limite esquerdo do mapa e no topo, a cima do jogador, o tipo kinematic significa que ela só colide com corpos dynamic, no caso só com o jogador
	self.body = love.physics.newBody(world, px, py, "kinematic")
	--criamos entao sua area de contato, vai ser o dobro da do jogador
	self.shape = love.physics.newChainShape(false, Coruja.vertices)
	--unimos o corpo a area de contato
	self.fixture = love.physics.newFixture(self.body, self.shape, 3)
  self.image = love.graphics.newImage("imagens/corujaImg.png") --carrega o sprite da coruja
	self.image:setFilter("nearest", "nearest") --seta o filtro de imagem sobre o sprite
	self.size = self.image:getHeight()	
  
  --animaçãoo
	local frames = anim8.newGrid(self.size, self.size, self.image:getWidth(), self.image:getHeight())
  --cria a animaçãoo do inimigo, que troca entre 5 frames ('1-5'), a cada 0.15seg
	self.animation = anim8.newAnimation(frames('1-5',1), 0.15) 
 
	--vx e vx são as variáveis que receberãoo uma velocidade em determinada direçãoo para fazer a coruja se movimentar
	self.vx = 0
	self.vy = 0
  
  return self
end

function Coruja:update(dt, velocidade) --atualiza a animação da coruja e sua movimentação
  self.animation:update(dt)
  self.body:setLinearVelocity(-velocidade, 0)
end

function Coruja:draw() --aqui desenhamos a coruja e sua area de contato
	love.graphics.setColor(255, 255, 255)
	self.animation:draw(self.image, self.body:getX() - 140, self.body:getY() - 130) 
	--love.graphics.setColor(255, 0, 0)    
  --love.graphics.polygon("line", self.body:getWorldPoints(self.shape:getPoints()))
end

function Coruja:respawn(scenarioPostionX)
	if self.body:getX() < (scenarioPostionX - self.image:getHeight()) then
			self.body:setPosition(
				scenarioPostionX + math.random(self.min_distance, self.max_distance),
				math.random(self.min_height, self.max_height)
			)
	end
end