local ifFile = require "source/input_fields"
local compFile = require "source/components"
local pontuacaoFile = require "source/pontuacao"

--DEFINES
local loveGraphics = love.graphics
local MARGING = 32
local COMPONENTS_HEIGHT = 48
local TEXT_MAX_LEN = 30
local TEXT_X = 40
local TITLE_MSG = "Submit Score to"
local USERNAME_LABEL = "username:"
local TOKEN_LABEL = "token:"
--END DEFINES

--LOCAL VARIABLES
local screenWidth
local screenHeigth

local fontLarge
local fontSmall

local startSmallTextY
local startSmallTextX

local sub_components = {}
local submit_fields = {}

local logoGJ
local menu
local SetKeyRepeat
--END LOCAL VARIABLES

local function joinTables(t1, t2)
   local tResult = {}
    
   for k,v in ipairs(t1) do
      table.insert(tResult, v)
   end
   
   for k,v in ipairs(t2) do
      table.insert(tResult, v)
   end 
   
   return tResult
end

function submit_create(font_path)
  
  fontLarge = loveGraphics.newFont(font_path, 64)
  fontSmall = loveGraphics.newFont(font_path, 27)
  
  screenWidth = loveGraphics.getWidth()
  screenHeight = loveGraphics.getHeight()
  
  SetKeyRepeat = love.keyboard.setKeyRepeat 
  SetTextInput = love.keyboard.setTextInput
  
  logoGJ = loveGraphics.newImage("imagens/logo_gamejolt.png")
                    
  startSmallTextY = fontSmall:getHeight()*1.5 + COMPONENTS_HEIGHT + fontLarge:getHeight()*1.5 + MARGING*2 + logoGJ:getHeight()
  
  menu = Menu('imagens/bg.png', font_path)
  
  ifield_add(submit_fields, 
            "", 
            USERNAME_LABEL, 
            fontSmall, 
            screenWidth/2, 
            startSmallTextY, 
            "username", 
            TEXT_MAX_LEN,
            "[%w.%-_]") 
  
  ifield_add(submit_fields, 
            "", 
            TOKEN_LABEL, 
            fontSmall, 
            screenWidth/2, 
            startSmallTextY + fontSmall:getHeight()*2 + MARGING,             
            "token", 
            TEXT_MAX_LEN,
            "%w")
  
	--botoes do menu
	local y = startSmallTextY + fontSmall:getHeight()*5 + MARGING
	menu:addButton("submit", 358, y, 'submit')
	menu:addButton("cancel", 370, y + 50, 'cancel')
	
	return menu
end

function submit_draw(score)
    love.graphics.setBackgroundColor(37, 170, 225) --muda a cor de fundo 
	menu:drawButtons()	
    love.graphics.setColor(255, 134, 218, 255) --rosa
	loveGraphics.setFont(fontLarge)
	loveGraphics.print(TITLE_MSG, 
					TEXT_X, 
					COMPONENTS_HEIGHT)
	loveGraphics.setColor(255, 255, 255)					
	loveGraphics.print(TITLE_MSG, 
					TEXT_X-3, 
					COMPONENTS_HEIGHT-3)  

	love.graphics.setColor(255, 255, 255)
	love.graphics.setFont(fontSmall)
	love.graphics.printf(score:tempoDeJogo(), 120, 150, 400, "left")
	love.graphics.printf(score:distanciaPercorrida(), 120, 200, 400, "left")
	love.graphics.printf(score:floresTocadas(), 290, 150, 400, "right")
	loveGraphics.setColor(104, 0, 147) -- purple
	love.graphics.printf(score:pontuacaoFinal(), 290, 200, 400, "right")					
	
	loveGraphics.setColor(255, 255, 255)
	loveGraphics.draw(logoGJ, screenWidth - TEXT_X - logoGJ:getWidth()*0.7, COMPONENTS_HEIGHT + logoGJ:getHeight()*0.35 - 8, 0, 0.7, 0.7)
	
	ifield_draw(submit_fields, {104, 0, 147}, {104, 0, 147}, {252, 126, 255})
end  

function submit_checkComponents(key)
	menu:checkSelection(key)
    ifield_check(submit_fields, key)
end 

function submit_clickOnComponents()
	menu:checkClick()
    ifield_click(submit_fields) 
end  

function submit_getInput(text)
    ifield_update(submit_fields, text)
end

function submit_getAuthentication()
	local auth = {
		user = ifield_getText(submit_fields, "username"), 
		token = ifield_getText(submit_fields, "token")
	}
	return auth
end

 