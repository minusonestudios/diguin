Credits = {}
Credits.__index = Credits

setmetatable(Credits, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

function Credits.new(fontPath)
	local self = setmetatable({}, Credits)
    self.font = love.graphics.newFont(fontPath, 45)
    self.superFont = love.graphics.newFont(fontPath, 50)
    self.masterFont = love.graphics.newFont(fontPath, 68)
    return self
end

function Credits:draw()
    love.graphics.setBackgroundColor(0, 100, 200)
    love.graphics.setColor(255, 255, 255)
    love.graphics.setFont(self.masterFont)
	love.graphics.print("A game by Minus One Studios", 10, 15)
    love.graphics.setFont(self.font)
	love.graphics.print("Developers", 314, 105)
    love.graphics.setFont(self.superFont)
	love.graphics.print("Kael Fraga, Pablo Diehl", 163, 170)
    love.graphics.setFont(self.font)
	love.graphics.print("Arts", 355, 245)
    love.graphics.setFont(self.superFont)
	love.graphics.print("Kael Fraga", 280, 300)
    love.graphics.setFont(self.font)
	love.graphics.print("Music", 344, 370)
    love.graphics.setFont(self.superFont)
	love.graphics.print("Johan Brodd", 275, 415)
    love.graphics.setFont(self.font)
	love.graphics.print("Sound Effects", 292, 490)
    love.graphics.setFont(self.superFont)
	love.graphics.print("Iwan Gabovitch, NenadSimic", 144, 540)
end