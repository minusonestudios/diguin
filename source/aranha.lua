local anim8 = require 'source/anim8'

Aranha = {
    aceleracao = 100,
    tamanho = 60,
    velocidade = 100,
    min_distance = 1000,
    max_distance = 2000
}
Aranha.__index = Aranha

setmetatable(Aranha, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

function Aranha.new(world, px)
    local self = setmetatable({}, Aranha)
	self.body = love.physics.newBody(world, px, 10, "kinematic")
	self.shape = love.physics.newCircleShape(self.tamanho)
	self.fixture = love.physics.newFixture(self.body, self.shape, 1)
    self.image = love.graphics.newImage("imagens/aranha.png")
    self.image:setFilter("nearest", "nearest")
    --self.web = love.graphics.newImage("imagens/web.png")
    --self.web:setFilter("nearest", "nearest")
	local frames = anim8.newGrid(
        self.image:getHeight(), self.image:getHeight(),
        self.image:getWidth(),
        self.image:getHeight()
    )
	self.animation = anim8.newAnimation(frames('1-4',1), 0.4)
    return self
end

function Aranha:update(dt, worldSize)
    self.animation:update(dt)
    if self.body:getY() >= (worldSize.height - 3.1 * self.tamanho) then
        self.aceleracao = -self.velocidade
    elseif self.body:getY() <= self.tamanho then
        self.aceleracao = self.velocidade
    end
    self.body:setLinearVelocity(0, self.aceleracao)
end

function Aranha:draw(dt)
    love.graphics.setColor(34, 34, 34)
    -- Este retângulo será a "raiz" da teia da Aranha
    love.graphics.rectangle(
        -- Usamos filtro Fill pois queremos que seja usada uma cor sólida
        "fill",
        -- Aqui definimos as coordenadas do retângulo, que será desenhado a partir do topo da tela, um pouquinho mais a direita da Aranha, para alinhar com a arte
        self.body:getX() + 1, 0, 
        -- O retangulo terá 2 pixels de largura e o comprimento se estenderá até a aranha
        2, self.body:getY()
    )
	love.graphics.setColor(255, 255, 255, 255)
    --[[love.graphics.draw(
        self.web,
        self.body:getX() - self.shape:getRadius() * 2.1,
        self.body:getY() - self.shape:getRadius() * 1.4
    )]]
    self.animation:draw(
        self.image,
        self.body:getX() - self.image:getHeight()/2,
        self.body:getY() - self.image:getHeight()/2
    )

    --love.graphics.setColor(150, 100, 50, 255)
	--love.graphics.circle("line",self.body:getX(),self.body:getY(),self.shape:getRadius())
end

function Aranha:respawn(scenarioPostionX)
    if self.body:getX() < (scenarioPostionX - self.image:getHeight()) then
        self.body:setPosition(scenarioPostionX + math.random(self.min_distance, self.max_distance), self.body:getY())
    end
end