local VELOCIDADE_MAXIMA = 450 -- A caixa deverá sempre ser mais lenta que o Diguin, que tem uma velocidade de 500
local RIGHT_BORDER_FIX = 0.95

Caixa = {}
Caixa.__index = Caixa

setmetatable(Caixa, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})
  
local function buildBorder(world, x1, x2, y1, y2)
    local border = {}
    --cria um corpo para a parede, no mundo do jogo, seu tipo seu tipo será kinematic pois ela movimentar-se-a lentamente para direita
    border.body = love.physics.newBody(world, 0, 0, "kinematic")
    --cria uma forma para a parede, nas coordenadas definidas
    border.shape = love.physics.newEdgeShape(x1, x2, y1, y2)
    --fixa a forma ao corpo, criando a area de colisao
    border.fixture = love.physics.newFixture(border.body, border.shape, 5)
    return border;
end
	
function Caixa.new(world, worldSize)
    local self = setmetatable({}, Caixa) --cria um registro com as caracteristicas da Caixa
  
    local width = worldSize.width
    local height = worldSize.height -- Altura da tela de jogo + margem pro porongo do diguin sair da tela
    
    self.velocidade = 100
  
    self.borders = {
        top = buildBorder(world, 5, 5, width * RIGHT_BORDER_FIX, 5),
        left = buildBorder(world, 5, 5, 5, height),
        right = buildBorder(world, width * RIGHT_BORDER_FIX, 5, width * RIGHT_BORDER_FIX, height),
        bottom = buildBorder(world, 5, height, width * RIGHT_BORDER_FIX, height)
    } 
    
    return self
end

--desenho das bordas, opcional, usado somente para verificar se as posicoes delas estao corretas
function Caixa:draw()
	for i, border in ipairs(self.borders) do --mesma mecânica que um "for each"
		love.graphics.setColor(255, 0, 0)
		love.graphics.line(border.body:getWorldPoints(border.shape:getPoints()))
		--desenha o limite como uma linha, body:getWorldPoint e o shape:getPoints retornam os valores definidos da parede na criacao
	end
end


-- A caixa deve mover-se automaticamente para a esquerda, a velocidade irá aumentar gradativamente
function Caixa:update(dt)
    -- Enquanto a  caixa não atingiu sua velocidade máxima, seguimos acelerando
    if self.velocidade < VELOCIDADE_MAXIMA then
        self.velocidade = self.velocidade + 1 * dt
    end
    
    self.borders.top.body:setLinearVelocity(self.velocidade, 0)
    self.borders.left.body:setLinearVelocity(self.velocidade, 0)
    self.borders.right.body:setLinearVelocity(self.velocidade, 0)
    self.borders.bottom.body:setLinearVelocity(self.velocidade, 0)
end