local componentsFile = require "source/components"
local BUTTON_WIDTH = 120
local MARGING = 10
local LINE_WIDTH = 5
local FIXED_X = 335
local GAME_TITLE = "Diguin Adventure"
local GAME_TITLE_X = 125
local GAME_TITLE_Y = 58
local AVATAR_X = 270
local AVATAR_Y = 128
local FONT_SIZE = 28
local MINUS_ONE = "-1 Studios"
local BRAND_X = 15
local BRAND_Y = 570

Menu = {} --comecamos criando um registro das propriedades do menu
Menu.__index = Menu

setmetatable(Menu, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

local function mouseHouver(button, menuFont)
	return components_mouseHover(
		FIXED_X,        
		button.y - (MARGING/2),
		FIXED_X + BUTTON_WIDTH,
		button.y + menuFont:getHeight() + MARGING
	)
end

function Menu.new(backgroundImage, fontPath)
	local self = setmetatable({}, Menu)  
	self.font = love.graphics.newFont(fontPath, FONT_SIZE) --criamos uma fonte padrao para o menu  
    self.titleFont = love.graphics.newFont(fontPath, 80) --criamos uma fonte padrao para o menu  
    self.brandFont = love.graphics.newFont(fontPath, 30)
    self.buttons = {} --criamos um registro para os botoes  
    self.select = 1 -- armazena posição do seletor
	self.backImage = love.graphics.newImage(backgroundImage) --carrega uma imagem para o fundo do menu
	return self
end

function Menu:addButton(text, x, y, id) --funcao que cria botoes, criaremos dois somente
	table.insert(self.buttons, {text=text, x=x, y=y, id=id, selected=false})
	--table insert eh uma funcao da lua para preencher uma tabela
	--text sera mensagem do botao
	--x e y sua posicao na tela
	--id sua identidade
	--e selected uma varivel para verificacao
	self.buttons[1].selected = true -- o primeiro botao ja surge selecionado
end

function Menu:drawArt ()
	-- Game Title
	love.graphics.setColor(0, 100, 200) -- azul
	love.graphics.setFont(self.titleFont)
	love.graphics.print(GAME_TITLE, GAME_TITLE_X, GAME_TITLE_Y)
	love.graphics.setColor(250, 250, 250) -- quase branco
	love.graphics.setFont(self.titleFont)
	love.graphics.print(GAME_TITLE, GAME_TITLE_X-5, GAME_TITLE_Y-5)
    
    love.graphics.setColor(0, 100, 200) -- azul
	love.graphics.setFont(self.brandFont)
    love.graphics.print(MINUS_ONE, BRAND_X, BRAND_Y)
    love.graphics.setColor(250, 250, 250)
    love.graphics.setFont(self.brandFont)
	love.graphics.print(MINUS_ONE, BRAND_X-1, BRAND_Y-1)
end

function Menu:drawButtons () --essa funcao percorre o registro criado e desenha cada botao
	--Background
	love.graphics.setColor(230, 230, 230) -- quase branco
	love.graphics.draw(self.backImage, 0, 0)
	
	-- Buttons
	for i,v in ipairs(self.buttons) do --ipairs eh um comando para retornar do registro um conjunto de valores associados a uma posicao
		if mouseHouver(v, self.font) then
      --quando o botao esta selecionado ficara com a cor rosa clara
			love.graphics.setColor(204, 0, 204) 
      --desenha um botao com tamanho suficiente para caber a mensagem
			love.graphics.rectangle("fill", FIXED_X, v.y - MARGING/2, BUTTON_WIDTH, self.font:getHeight()+MARGING) 
		else
			love.graphics.setColor(102, 0, 102) --quando o botao nao esta selecionado ficara com a cor rosa escuro
			love.graphics.rectangle("fill", FIXED_X, v.y - MARGING/2, BUTTON_WIDTH, self.font:getHeight()+MARGING)
		end

		if v.selected == true then
			--quando o botao esta selecionado ficara com a cor rosa clara
			love.graphics.setColor(204, 0, 204)
			love.graphics.setLineWidth(LINE_WIDTH)
			love.graphics.rectangle("line", FIXED_X, v.y - MARGING/2, BUTTON_WIDTH, self.font:getHeight() + MARGING)
		end

		love.graphics.setColor(255, 255, 255)--define a cor da mensagem
		love.graphics.setFont(self.font)--chama a fonte padrao
		love.graphics.print(v.text, v.x, v.y) --escreve a mensagem nas posicoes escolhidas
	end
end

function Menu:checkSelection(key) --verfica se o botao esta em selecao
	if key=="up" then --caso o usuario aperte a tecla UP, o cursor sobe uma posição
        if self.select > 1 then
            self.select = self.select - 1
        else
            self.select = #self.buttons
        end
	elseif key=="down" then --caso o usuario aperte a tecla DOWN, o cursor desce uma posição
        if self.select < #self.buttons then
            self.select = self.select + 1
        else
            self.select = 1
        end
	end
    for i=1, #self.buttons do
        if self.select == i then
            self.buttons[i].selected = true
        else
            self.buttons[i].selected = false
        end
    end
end

function Menu:checkClick() --verfica se o button foi clicado
	components_deselectAll(self.buttons)
	for i,v in ipairs(self.buttons) do	
		if mouseHouver(v, self.font) then          
			v.selected = true  
    end     
	end
end