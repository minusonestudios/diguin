local GetMousePosition = love.mouse.getPosition

function components_clear(components) --funcao que destroi botoes
    for i,v in ipairs(components) do --for each component
        components[i] = nil
    end
end

function components_deselectAll(components)
   for i,v in ipairs(components) do --for each component
        v.selected = false
    end
end

function components_mouseHover(componentX1, componentY1, componentX2, componentY2)
    x, y = GetMousePosition()
    if x > componentX1 and
       x < componentX2 and
       y > componentY1 and
       y < componentY2 then  
        return true  
    else
        return false
    end 
end 

function components_select(components, key) --verfica se o component em selecao
  if key=="tab" and #components > 1 then 
    local flag_SomeoneSelected = false
    for i,v in ipairs(components) do
      if v.selected then
          if (i + 1) <= #components then
             components[i + 1].selected = true
             flag_SomeoneSelected = true
             v.selected = false             
             break
          else
             components[1].selected = true
             flag_SomeoneSelected = true
             v.selected = false
             break
          end  
      end
    end 
    if not flag_SomeoneSelected then
      components[1].selected = true  
    end
  end 
end
