Pontuacao = {
    distancia = 0,
    flores = 0,
    inicioPeriodo = 0,
    fimPeriodo = 0,
    px = 0,
    py = 550,
    tempoTotal = 0,
    total = 0,
    font = nil
}
Pontuacao.__index = Pontuacao

setmetatable(Pontuacao, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})
	
function Pontuacao.new(fontPath)
    local self = setmetatable({}, Pontuacao)
    self.font = love.graphics.newFont(fontPath, 42)
    return self
end

function Pontuacao:update(jogadorX)
    -- Se o jogador avançou, atualize a distancia
    local distanciaAtual = math.floor(jogadorX / 1000)
    if distanciaAtual > self.distancia then
        self.distancia = distanciaAtual
    end
    self.total = self.distancia + self.flores
end

function Pontuacao:draw(caixaX)
    love.graphics.setFont(self.font)
    love.graphics.setColor(255, 134, 218, 255) --rosa
    self.px = (caixaX + love.graphics.getWidth()/2) - (self.font:getWidth(tostring(self.total))/2)
    self.py = 100
    love.graphics.print(self.total, self.px, self.py)
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.print(self.total, self.px - 1.5, self.py - 1.5)
end

function Pontuacao:pegaFlor()
    self.flores = self.flores + 50
end

-- Inicia um periodo de contagem de tempo
function Pontuacao:iniciarCronometro()
    self.inicioPeriodo = love.timer.getTime( )
end

-- Para de contar o tempo (ao morrer ou retornar ao menu)
function Pontuacao:pararCronometro()
    self.fimPeriodo = love.timer.getTime()
    self.tempoTotal = self.tempoTotal + (self.fimPeriodo - self.inicioPeriodo)
end

-- Retorna o tempo que o jogador permaneceu vivo em jogo
function Pontuacao:tempoDeJogo()
    return "Time survived: " .. math.floor(self.tempoTotal) .. " seconds"
end

function Pontuacao:distanciaPercorrida()
    return "Flight distance: " .. self.distancia .. " meters"
end

function Pontuacao:floresTocadas()
    return "Flowers: " .. self.flores .. " points"
end

function Pontuacao:pontuacaoFinal()
    self.total = self.distancia + math.floor(self.tempoTotal) + self.flores
    return "Final Score: " .. self.total
end

function Pontuacao:getMain()
   return self.distancia + math.floor(self.tempoTotal) + self.flores
end