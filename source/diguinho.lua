local anim8 = require 'source/anim8' --importa a biblioteca do love responsavel pelas animaçoes

Jogador = {
    velocidade = 500,
    vertices = {57.530, 129.863,
                75.222, 121.843,
                103.713, 99.159,
                110.886, 75.711,
                120.987, 57.479,
                107.241, 53.297,
                92.435, 70.652,
                70.231, 99.976,
                },
    -- Uma trollagem, o Diguin voa mais rápido pra trás do que para frente
    superVelocidade = 750
}
Jogador.__index = Jogador

setmetatable(Jogador, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

function Jogador.new(world, x, y) --instancia as propriedades do jogador
	--mesmo processo feito com o jogador
	local self = setmetatable({}, Jogador) --criamos um registro para guardar as informacoes do jogador
	self.body = love.physics.newBody(world, x, y, "dynamic")--criamos o corpo dele no mundo, na posicao (100,300) do tipo dinamico, ou seja ele se movimenta
  self.body:setFixedRotation(true)
  self.shape = love.physics.newPolygonShape(Jogador.vertices)
  --self.shape = love.physics.newCircleShape(30) --cria uma hit 'box' para o jogador, na verdade eh um circulo como area de contato
	self.fixture = love.physics.newFixture(self.body, self.shape, 1)--unimos o corpo com a area de contato
	self.alive = true --cria uma variavel que carrega a informacao que o jogador esta vivo ou morto
  self.image = love.graphics.newImage("imagens/diguinho.png") --carrega o sprite de animacao
  self.image:setFilter("nearest", "nearest") --define o filtro de interpolacao da imagem, mudando sua textura
	local frames = anim8.newGrid(self.image:getHeight(), self.image:getHeight(), self.image:getWidth(), self.image:getHeight())
	self.animation = anim8.newAnimation(frames('1-5',1), 0.07) --cria a animacao do jogador
  
  return self
end

function Jogador:update(dt) --funcao que atualiza o jogador, a sua animacao e sua movimentacao
	self.animation:update(dt) 
	--certas operacoes sao podem ocorrer com o jogador vivo, pois com ele morto alem de nao fazerem sentido, ocorrerah um erro
	if self.alive then
    if love.keyboard.isDown("right","left","up","down") then
      if love.keyboard.isDown("right") and love.keyboard.isDown("up") then
        self.body:setLinearVelocity(Jogador.velocidade, -Jogador.velocidade)
      elseif love.keyboard.isDown("left") and love.keyboard.isDown( "up") then
        self.body:setLinearVelocity(-Jogador.superVelocidade, -Jogador.velocidade)
      elseif love.keyboard.isDown("left") and love.keyboard.isDown( "down") then
        self.body:setLinearVelocity(-Jogador.superVelocidade, Jogador.velocidade)
      elseif love.keyboard.isDown("right") and love.keyboard.isDown( "down") then
        self.body:setLinearVelocity(Jogador.velocidade, Jogador.velocidade)
      elseif love.keyboard.isDown("right") then
        self.body:setLinearVelocity(Jogador.velocidade, 0)
      elseif love.keyboard.isDown("left") then
        self.body:setLinearVelocity(-Jogador.superVelocidade, 0)
      elseif love.keyboard.isDown("up") then
        self.body:setLinearVelocity(0, -Jogador.velocidade)
      elseif love.keyboard.isDown("down") then
        self.body:setLinearVelocity(0, Jogador.velocidade)
      end
    else
      self.body:setLinearVelocity(0,0) --caso o usuario nao esteja pressionando nenhuma tecla o jogador para
    end
	end
end

function Jogador:draw() --essa funcao desenha a animacao do jogador e sua forma se quiser conferir o posicionamento
	if self.alive then
		love.graphics.setColor(255, 255, 255,255)--definimos a cor branca para preservar a cor original da imagem
		--desenhamos a animacao do jogador
		self.animation:draw(self.image, self.body:getX(), self.body:getY())						
		--desenhamos a area de contato para posiciona-la adequadamente, depois comentamos
		--love.graphics.setColor(0, 0, 255)    
    --love.graphics.polygon("line", self.body:getWorldPoints(self.shape:getPoints()))
	end
end
