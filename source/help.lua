Help = {}
Help.__index = Help

setmetatable(Help, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

function Help.new(fontPath)
	local self = setmetatable({}, Help)
    self.font = love.graphics.newFont(fontPath, 45)
    self.superFont = love.graphics.newFont(fontPath, 60)
    return self
end

function Help:draw()
    love.graphics.setBackgroundColor(0, 100, 200)
    love.graphics.setColor(255, 255, 255)
    love.graphics.setFont(self.superFont)
    love.graphics.print("How to Play", 250, 100)
    love.graphics.setFont(self.font)
	love.graphics.print("Use the arrow keys to control the character", 72, 180)
    love.graphics.setFont(self.superFont)
	love.graphics.print("Objective", 307, 300)
    love.graphics.setFont(self.font)
	love.graphics.print("Fly as far as possible, avoiding the enemies", 70, 380)
    love.graphics.print("Touch the flowers to get extra points", 129, 450)
end