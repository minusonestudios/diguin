Cactus = {
    vertices = {
          1, 330,
          2, 117,
         26, 79,--59,
        100, 71,--51,
        131, 119,
        164, 157,
        157, 183,
        121, 330
    },
    min_distance = 1300,
    max_distance = 3600,
    y = 300
}
Cactus.__index = Cactus

setmetatable(Cactus, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

function Cactus.new(world, px)
    local self = setmetatable({}, Cactus)
	self.body = love.physics.newBody(world, px, Cactus.y, "kinematic")
	self.shape = love.physics.newPolygonShape(
        self.vertices
    )    
	self.fixture = love.physics.newFixture(self.body, self.shape, 1)
    self.image = love.graphics.newImage("imagens/cactus.png")
    self.image:setFilter("nearest", "nearest")
    return self
end

function Cactus:draw(dt)
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.draw(
        self.image,
        self.body:getX(),
        self.body:getY() + 70
    )
    --love.graphics.setColor(0, 255, 255)
    --love.graphics.polygon("line", self.body:getWorldPoints(self.shape:getPoints()))
end

function Cactus:respawn(scenarioPostionX)
    if self.body:getX() < (scenarioPostionX - self.image:getWidth()) then
        self.body:setPosition(scenarioPostionX + math.random(self.min_distance, self.max_distance), self.body:getY())
    end
end