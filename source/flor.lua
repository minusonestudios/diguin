local FIX_IN_X = 105
local FIX_IN_Y = 110

Flor = {
  max_distance = 1000
}
Flor.__index = Flor

setmetatable(Flor, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

local function randcolor()
  return love.math.random(1, 255)
end

function Flor.new(world, positionX) --criacao da flor, o objetivo final do jogo
  --mesmo processo feito com o jogador
  local self = setmetatable({}, Flor)
	self.body = love.physics.newBody(world, positionX, love.graphics.getWidth()-470, "static")--no caso da flor a area de contato sera so o miolo
	self.shape = love.physics.newCircleShape(40)
	self.fixture = love.physics.newFixture(self.body, self.shape, 1)
  self.flowerBody = love.graphics.newImage( 'imagens/flor.png' )
  self.flowerPetals = love.graphics.newImage( 'imagens/petalas.png' )
  self.active = true -- deterina se haverá pontuação ao tocar a flor
  local r, g, b = randcolor(), randcolor(), randcolor() 
  self.color = {r, g, b}
  
  return self
end

function Flor:getWidth()
  return self.flowerPetals:getWidth()
end

function Flor:getHeight()
  return self.flowerBody:getHeight()
end

function Flor:draw() --desenhamos a flor
  --procuramos valores para posicionar a imagem com o miolo adequadamente
  love.graphics.setColor(self.color)
  love.graphics.draw(self.flowerPetals, self.body:getX()-FIX_IN_X, self.body:getY()-FIX_IN_Y)
  love.graphics.setColor(255,255,255)
  love.graphics.draw(self.flowerBody, self.body:getX()-FIX_IN_X, self.body:getY()-FIX_IN_Y)
  --love.graphics.setColor(0, 0, 0, 255)
	--love.graphics.circle("line",self.body:getX(),self.body:getY(),self.shape:getRadius())
end

function Flor:respawn(scenarioPostionX)
  if self.body:getX() < (scenarioPostionX - self:getWidth()) then
    min_distance = love.graphics.getWidth() + self:getWidth()
    self.body:setPosition(scenarioPostionX + math.random(min_distance, self.max_distance), self.body:getY())
    self.active = true
    self.color = {randcolor(), randcolor(), randcolor()}
  end
end