local componentsFile = require "source/components"
local utf8 = require "utf8"

local FIELDS_WIDTH = 700
local MARGING = 16
local LINE_WIDTH = 3
local GetMousePosition = love.mouse.getPosition
local loveGraphics = love.graphics

--texto: mensagem do input_fields
--font: fonte do texto
--x e y: posição na tela
--selected: varivel para verificacao
function ifield_add(input_fields, texto, label, font, x, y, id, limit, pattern, input_type) --funcao que cria campos de edição, criaremos dois somente
	table.insert(input_fields, {texto = texto, 
                              label = label, 
                              font = font, 
                              x = x, y = y, 
                              id = id, 
                              limit = limit, 
                              pattern = pattern,
                              input_type = (input_type or "text"),
                              selected = false})
end

function ifield_clear(input_fields) --funcao que destroi campos de edição
    components_clear(input_fields)
end

local function mouseHover(single_input_field)
   return components_mouseHover(single_input_field.x - (FIELDS_WIDTH/2),        
                                single_input_field.y - (MARGING/2),
                                single_input_field.x + (FIELDS_WIDTH/2),
                                single_input_field.y + single_input_field.font:getHeight() + MARGING)
end 

function ifield_draw(input_fields, fontColor, backColor, highlightColor) --essa funcao percorre o registro criado e desenha cada input_fields
  for i,v in ipairs(input_fields) do --for each input fields
	if mouseHover(v) then
		--quando o input fields esta selected ficara com a cor clara  
		loveGraphics.setColor(highlightColor) 
		--desenha um input fields com tamanho suficiente para caber a mensagem
		loveGraphics.rectangle("fill", v.x - (FIELDS_WIDTH/2), v.y - MARGING/2, FIELDS_WIDTH, v.font:getHeight() + MARGING)       
    else
		loveGraphics.setColor(255, 255, 255) --quando o input fields nao esta selected ficara com a cor escura
		loveGraphics.rectangle("fill", v.x - (FIELDS_WIDTH/2), v.y - MARGING/2, FIELDS_WIDTH, v.font:getHeight() + MARGING)
		loveGraphics.setColor(backColor) --quando o input fields nao esta selected ficara com a cor escura
		loveGraphics.rectangle("line", v.x - (FIELDS_WIDTH/2), v.y - MARGING/2, FIELDS_WIDTH, v.font:getHeight() + MARGING)
	end
    
    if v.selected then
      loveGraphics.setColor(highlightColor)
      loveGraphics.setLineWidth(LINE_WIDTH)
      loveGraphics.rectangle("line", v.x - (FIELDS_WIDTH/2), v.y - MARGING/2, FIELDS_WIDTH, v.font:getHeight() + MARGING)
    end 
     
    --define a cor da mensagem
    loveGraphics.setColor(fontColor)
    --chama a fonte padrao
    loveGraphics.setFont(v.font)    
    --escreve a mensagem
    if v.label ~= nil then 
      loveGraphics.print(v.label, v.x - (FIELDS_WIDTH/2) + MARGING/2, v.y - v.font:getHeight() - MARGING)
    end    
    if v.texto ~= nil then
        local display_string = v.texto
        if v.input_type == "password" then
            display_string = string.rep("*", string.len(display_string))
        end 
        loveGraphics.printf(display_string, 
                             v.x - (FIELDS_WIDTH/2) + MARGING, 
                             v.y + 1, 
                             FIELDS_WIDTH)
    end  
  end
end

function ifield_select(input_fields, key) --verfica se o input fields em selecao
  components_select(ifield_click, key)
end

function ifield_check(input_fields, key)    
    local current_input = nil
    for i,v in ipairs(input_fields) do
      if v.selected then
          current_input = v          
          break  
      end
    end
    
    if current_input ~= nil and current_input.texto ~= nil then
      if key == "backspace" then
        -- get the byte offset to the last UTF-8 character in the string.
        local byteoffset = utf8.offset(current_input.texto, -1) 
        if byteoffset then
          -- remove the last UTF-8 character.
          -- string.sub operates on bytes rather than UTF-8 characters, so we couldn't do string.sub(text, 1, -2).
          current_input.texto = string.sub(current_input.texto, 1, byteoffset - 1)
        end        
      end
    end
end  

function ifield_click(input_fields) --verfica se o input fields foi clicado
	for i,v in ipairs(input_fields) do
		if mouseHover(v) then          
        v.selected = true
    else
        v.selected = false
    end  
	end
end

function ifield_update(input_fields, text)
  for i,v in ipairs(input_fields) do
		if v.selected == true and string.len(v.texto) < v.limit then 
        if v.pattern ~= nil and text:match(v.pattern) then
            v.texto = v.texto .. text
        end  
    end  
	end   
end  

function ifield_getText(input_fields, id)
	for i,v in ipairs(input_fields) do
		if v.id == id then 
			return v.texto
		end  
	end  
	return nil
end
  
function ifield_setText(input_fields, id, text)
  for i,v in ipairs(input_fields) do
		if v.id == id then 
        v.texto = text
    end  
	end  
end  
  